import pandas as pd
import numpy as np
from scipy.sparse.linalg import svds
from sklearn.decomposition import TruncatedSVD
import datetime
from timeit import default_timer as timer
import sys

def cargar_df_pickle(ruta):#,drop_list):
    df = pd.read_pickle(ruta)
    #df.drop(drop_list, inplace=True)
    return df

def entropy(df_csr):

    pc = df_csr.multiply(1/df_csr.sum(axis=1))
    pc.data = pc.data.astype(float)
    lpc = pc.copy()
    lpc.data = np.log(lpc.data)
    H = pc.multiply(lpc).sum(axis=1).astype(float)
    return H

def main():
    start = timer()

    #ruta = 'df_full_sparse_droped.pickle'
    ruta = sys.argv[1]
    filename = ruta.split('/')[-1]
    #drop_list = ['hyfen','punto']
    print('#cargando datos')
    #df = cargar_df_pickle(ruta)#,drop_list)
    df= pd.read_pickle(ruta)
    print('df: ',df.shape)
    #df.to_pickle('df_sparse_full_droped.pickle')
    print('#transformando csr')
    df_csr = df.sparse.to_coo().tocsr()    
    ###
    print('### eliminando zeros')
    #i = np.where(df_csr == 0)
    #df_csr.eliminate_zeros()
    #i_ = np.where(df_csr == 0)
    #print(i,i_)
    print('## buscando zeros a mano')
    aux = df_csr.multiply(1/df_csr.sum(axis=1)).astype(float)
    drops = []
    zeros_column_to_drop = df.index[aux.row[np.where(aux.data==0)[0]]].unique()
    drops.extend(zeros_column_to_drop)
    ones_column_to_drop = df.index[aux.row[np.where(aux.data>=1)[0]]].unique()
    drops.extend(ones_column_to_drop)
    #i = df.index[aux.row[np.where(aux.data==0)[0]]]
    print('# words to eliminate: {}'.format(len(drops)))
    print('# eliminando..')
    if len(drops)>0:
        del df_csr
        df.drop(drops, inplace=True)
        del drops
        df.to_pickle(ruta+'_droped_zeros_or_uniques.pkl')
        df_csr = df.sparse.to_coo().tocsr()
    
    ### aplicar filtro
    # ns
    drop_list= []
    nc = 50
    menos_nc_en_corpus = df.index[np.where(df_csr.sum(axis=1) < nc)[0]].tolist()

    ncc = 40
    ones = df_csr.copy()
    ones.data = np.array([1 for i in range(len(ones.data))])
    menos_ncc_contexts = df.index[np.where(ones.sum(axis=1) < ncc)[0]].tolist()
    
    drop_list.extend(menos_nc_en_corpus)
    drop_list.extend(menos_ncc_contexts)
    drop_list = list(set(drop_list))
    print('## drops by rules: ',len(drop_list))
    if len(drop_list) > 0:
        print('## droping..')
        df.drop(drop_list, inplace=True)
        df.to_pickle(ruta+'_droped-rules.pkl')
        del drop_list
        del df_csr
        df_csr = df.sparse.to_coo().tocsr()

    df_index = df.index
    df_columns = df.columns
    del df    


    #if i[0] > 0:
    #
    #    indices = list(set([df_csr.indices[j] for j in i[0]]))
    #    print('## indices a dropear: ', len(indices))
    #    shape_antes = df.shape
    #    df.drop(indices, inplace=True)    
    #    shape_despues = df.shape
    #    df_csr = df.sparse.to_coo().tocsr()
    #    print('## df antes: {}, df después: {}'.formate(shape_antes,shape_despues))
    #else:
    #    print('## no hay indices que dropear')
    #
    print('#calculando entropía')
    H = entropy(df_csr)
    print('#creando matriz')
    M = df_csr.log1p().multiply(1/H)

    print('#SVDS scipy')
    u, s, vt = svds(M,k=300)

    print('#guardando matrices de componentes')
    w = np.dot(u,np.diag(s))
    c = np.dot(np.diag(s),vt).T

    df_words = pd.DataFrame(w,index=df_index)
    print('#guardando palabras')
    df_words.to_csv(filename+'matriz_words_svds_300.csv')
    del df_words
    print('#guardando contextos')
    df_contexts = pd.DataFrame(c,index=df_columns)
    df_contexts.to_csv(filename+'matriz_contexts_svds_300.csv')
    print('#.')
    del df_contexts
   # del df
    del H
    
    #print
    print('#TruncatedSVD sklearn')

    tsvd = TruncatedSVD(n_components=300)
    
    print('#guardando palabras')

    words = tsvd.fit_transform(M)
    df_words = pd.DataFrame(words, index=df_index)
    df_words.to_csv(filename+'matriz_words_truncatedsvd_300.csv')
    print('#guardando contextos')

    contexts = tsvd.fit_transform(M.transpose())
    df_contexts = pd.DataFrame(contexts, index=df_columns)
    df_contexts.to_csv(filename+'matriz_contexts_truncatedsvd_300.csv')

    print('#Done!')

    end = timer()
    t_f = (end - start)/60
    print(t_f, 'mins.') 

    return True



if __name__ == '__main__':
    main()
