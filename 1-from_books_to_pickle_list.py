import pandas as pd
import numpy as np
import glob
from os import system, name 
import pickle
import sys
from timeit import default_timer as timer


from time import sleep 


def clear(): 
    if name == 'posix': 
        _ = system('clear') 
    
    else: 
        _ = system('clear')

def asignar_context(data, columna ='b_id', sep=1000, nom_c='c'):
    exit_df = pd.DataFrame({nom_c : []})
    #uni = data[columna].unique().tolist()

    grouped_data = data.groupby(columna)
    
    for n, u in grouped_data:
        s = int(len(u)/sep)
        if s >= 1:
            for x in range(0,s):
                indices = u.iloc[x*sep:x*sep+sep].index.tolist()
                aux_df = pd.DataFrame({nom_c: [str(n)+'-'+str(x)]*len(indices)} , index=indices)
                exit_df = exit_df.append(aux_df)
            
        
    return exit_df

def super_multiload(ruta_carpeta, lista_stop_words,sepa=1000, names = ['b_id','w_id','w','l','t'], col='l',b_id='b_id',ext= 'txt', sep='\t', max_=None):
    rutas = glob.glob(ruta_carpeta+'/*.'+ext)
    #df = pd.DataFrame({})
    l_df = []
    try:
      b = 0  
      for n, r in enumerate(rutas):
        df_ = pd.read_csv(r, sep=sep, encoding='latin-1',names=names)
        
        drop_cols = [c for c in df_.columns if c not in [col,b_id]]
        
        df_.drop(drop_cols, axis=1)

        print('### dropping stop words')

        no_alphas = df_[df_[col].str.isalpha() != True][col].tolist()
        #print(df_.head())
        ## identificate stop words
        
        lista_stop_words.extend(no_alphas)
        lista_stop_words = list(set(lista_stop_words))
        df_sw = pd.DataFrame(index=lista_stop_words)
        stopers = df_.merge(df_sw, how='inner', left_on=col, right_index=True)
        print('drops: ',len(stopers))
        print(len(df_))
        df_.drop(stopers.index,axis=0, inplace=True)
        print(len(df_))
        del stopers
        del no_alphas
        print('## creating contexts')
        nom_c = 'c'
        df_c = asignar_context(df_,sep=sepa, nom_c= nom_c)
        print()
        print('## assigni context to each word')
        df_ = df_.merge(df_c, left_index=True, right_index=True, how='inner', copy=False)
        print('#####',str(len(df_)))
        del df_c    
        contexts = df_[nom_c].unique().tolist()
        lencontexts = len(contexts)
        
        print('## creando vectores de frecuencia')
        lnc = len(contexts)

        grouped_by_contexts = df_.groupby(nom_c)
        del df_

        for nn, c in enumerate(grouped_by_contexts):
            print('#contando...')
            print(len(c[1]))
            vc = c[1][col].value_counts()
            vc = vc[vc>0]
            #print('#sparceando...')            
            vec = pd.DataFrame({c[0]:vc.values}, index=vc.index).astype('uint8')
            print(len(vec))
            del vc
            print('#dtype... ',vec.dtypes)
            #df = df.merge(vec, how='outer', right_index=True, left_index=True, copy=False)
            print('#appendiando')
            l_df.append(vec)
            del vec
            #print('sparseando..')
            #df[c] = df[c].astype(pd.SparseDtype('uint8',np.nan), copy=False)
            print('#'+str(nn)+'-'+str(lnc))
            
            if nn%10 == 0:
                clear() 

        #del df_
        del grouped_by_contexts
        b = b+lencontexts
        print('loaded {} of {} files - total contexts: {}'.format(n+1,len(rutas),b))
    except KeyboardInterrupt:
      return l_df
    
    return l_df


def main():
    start = timer()
    sepa = 500
    save_to_path = 'wlp_lat-es-eu_{}.pkl'.format(sepa)
    stop_words_path = 'drive-download-20190722T185210Z-001/stopwords.txt'
    add_to_list = ['y','el','la','los','las','nosotros','vosotros','ellos','ellas']
    where_to_look = 'drive-download-20190722T185210Z-001/wlp/*/'

    # load stopwords
    sw = pd.read_csv(stop_words_path, sep='\t', encoding='latin-1', names=['sw'])
    # improve stopwords

    list_sw = sw['sw'].tolist()

    list_sw.extend(add_to_list)
    list_sw = list(set(list_sw))

    l_df = super_multiload(where_to_look, lista_stop_words=list_sw, sepa=sepa)



    with open(save_to_path, 'wb') as f:
        pickle.dump(l_df, f)
    
    end = timer()
    t_f = (end - start)/60
    print(t_f, 'mins.') 



if __name__ == '__main__':
    main()
