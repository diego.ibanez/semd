import pandas as pd
import numpy as np
import sys
from timeit import default_timer as timer

import pickle


from time import sleep 


def concat_batchs(lista, criterio='raiz_cuadrada'): 
    l = [] 
    #df = pd.DataFrame([])
    ll = len(lista) 
    if criterio =='raiz_cuadrada':
        s = int(np.sqrt(ll)) 
        #print(ll,s)
        b = s

    
    #df = pd.DataFrame({})
    if ll > 3: 
        for i in range(0,s): 
            print('joining')
            df = pd.DataFrame([])
            df = df.join(lista[i*b:i*b+b], how='outer').astype(pd.SparseDtype('uint8',np.nan), copy=False) 
            #print('appending reducing') 
            #l.append(df.astype(pd.SparseDtype('uint8', np.nan))) 
            l.append(df) 
            print('apended: {} - {}'.format(b*i+b,ll)) 
       
     
        if ll%b != 0: 
           df = pd.DataFrame({}) 
           df = df.join(lista[s*b:], how='outer') 
           l.append(df) 
           #del df 
        return l 

    else:
        df = pd.DataFrame([])
        df = df.join(lista, how='outer').astype(pd.SparseDtype('uint8',np.nan), copy=False) 
        print('##DONE JOINING: ',df.shape)
        return df
 


#filename= 'wlp_total.pkl'


def main():
    start = timer()
    
    filename = sys.argv[1]

    print('#geting vectors')
    with open(filename, 'rb') as handle:
        v_l = pickle.load(handle)
    l_df = v_l
    #
    #v_l = v_l[:100]
    #
    #df = pd.concat(v_l, axis=1, join='outer', copy=False,sort=False)
    #for n, v in enumerate(v_l):
    #    df = df.merge(v, how='outer', left_index=True, right_index=True, copy=False)
    #    if n%500 == 0:
    #        print(n)
    print('#initialization loop')
    while True:
        l_df = concat_batchs(l_df)
        print('#new list of: ',len(l_df))
        if type(l_df) == pd.core.frame.DataFrame :
            #return l_df
            del v_l
            print('df of: ',l_df.shape)
            l_df.to_pickle('df_sparse_frec_'+filename+'.pkl')
            break
    
    end = timer()
    t_f = (end - start)/60
    print(t_f, 'mins.') 

if __name__ == '__main__':
    main()
